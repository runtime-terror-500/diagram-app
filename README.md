# DiagramApp

A web app for editing and viewing architecture diagrams defined by the specification in [rt-lib](https://gitlab.com/runtime-terror-500/rt-lib).

## Development server

Run `docker-compose up` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `docker-compose exec web-app ng generate component component-name` to generate a new component. You can also use `docker-compose exec web-app npm run ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `docker-compose exec web-app ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

  Run `docker-compose exec web-app npm run test-ci` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `docker-compose exec web-app ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `docker-compose exec web-app ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
