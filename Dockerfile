FROM node:12
RUN apt-get update
RUN apt-get install chromium -y
ENV CHROME_BIN /usr/bin/chromium
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
RUN npm install -g @angular/cli
COPY . /usr/src/app
CMD npm run start
