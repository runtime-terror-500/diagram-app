import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ParentComponent} from './main/parent/parent.component';
import {SplashComponent} from './splash/splash.component';

const routes: Routes = [
  { path: 'editor', component: ParentComponent },
  { path: 'splash', component: SplashComponent },
  { path: '', redirectTo: '/splash', pathMatch: 'full'},
  { path: '**', redirectTo: '/splash', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
