import { TestBed } from '@angular/core/testing';

import { MetadataDisplayService } from './metadata-display.service';

describe('MetadataDisplayService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MetadataDisplayService = TestBed.get(MetadataDisplayService);
    expect(service).toBeTruthy();
  });
});
