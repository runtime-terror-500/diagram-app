import { Injectable } from '@angular/core';
import { GraphComponent } from '../d3/models/component.model';
import { Relationship } from '../d3/models/relationship.model';
import { Connection } from '../d3/models/connection.model';

@Injectable({
  providedIn: 'root',
})

export class MetadataDisplayService {
  selectedComponent: GraphComponent = null;
  selectedConnection: Connection = null;
  components: GraphComponent[] = [];

  clear() {
    this.selectedConnection = null;
    this.selectedComponent = null;
    this.components = [];
  }

  displayComponent(component: GraphComponent) {
    this.clear();
    this.selectedComponent = component;
  }

  displayConnection(connection: Connection, components: GraphComponent[]) {
    this.clear();
    this.selectedConnection = connection;
    this.components = components;
  }
}
