import { Component, OnInit } from '@angular/core';
import { MetadataDisplayService } from './metadata-display.service';
import { ParentService } from '../parent/parent.service';

@Component({
  selector: 'app-metadata-display',
  templateUrl: './metadata-display.component.html',
  styleUrls: ['./metadata-display.component.scss']
})
export class MetadataDisplayComponent implements OnInit {

  objectKeys = Object.keys;

  constructor(public metadataService: MetadataDisplayService,
              private parentService: ParentService) {}

  ngOnInit() {
    this.parentService.getKeyUpListener().subscribe(yaml => {
      this.metadataService.clear();
    });
  }

}
