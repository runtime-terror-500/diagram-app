import {Component, OnDestroy, OnInit} from '@angular/core';
import {ToolbarService} from '../../toolbar/toolbar.service';
import {Subject, Subscription} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {ParentService} from '../parent/parent.service';

@Component({
  selector: 'app-text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.scss']
})
export class TextEditorComponent implements OnInit, OnDestroy {

  private fileUploadListener: Subscription;
  private fileDownloadListener: Subscription;

  keyUp = new Subject();

  private subscription: Subscription;

  editorOptions = {theme: 'vs-dark', language: 'yaml'};
  code = '';

  constructor(private toolbarService: ToolbarService,
              private parentService: ParentService) { }

  ngOnInit() {
    this.subscription = this.keyUp.pipe(debounceTime(1500)).subscribe(result => {
      this.parentService.emitKeyUpEvent(this.code);
    });

    this.fileUploadListener = this.toolbarService.getFileUploadListener().subscribe(yaml => {
      this.code = yaml;
      this.parentService.emitKeyUpEvent(this.code);
    });
    this.fileDownloadListener = this.toolbarService.getFileDownloadListener().subscribe(action => {
      if (action) {
        this.downloadFile();
      }
    });
  }

  private downloadFile() {
    this.toolbarService.downloadFile(this.code);
  }

  ngOnDestroy() {
    this.fileUploadListener.unsubscribe();
    this.fileDownloadListener.unsubscribe();
    this.subscription.unsubscribe();
  }
}
