import {AfterViewInit, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {MatSelectModule} from '@angular/material/select';
import {Graph} from '../models/graph.model';
import {D3Service} from '../services/d3.service';
import {GraphComponent} from '../models/component.model';
import {Connection} from '../models/connection.model';
import {Relationship} from '../models/relationship.model';
import {Options} from '../../../shared/options.interface';
import {ConnectionPoint} from './connection-point.interface';
import {UseCase} from '../models/usecase.model';
import {UseCaseDisplayService} from '../../usecase-display/use-case-display.service';
import {Subscription} from 'rxjs';



@Component({
  selector: 'app-graph-visual',
  templateUrl: './graph-visual.component.html',
  styleUrls: ['./shared-visual.component.scss']
})
export class GraphVisualComponent implements OnInit, AfterViewInit {

  /* tslint:disable */
  private _options: Options;
  private _components: GraphComponent[];
  private _relationships: Relationship[];
  private connections: Connection[];
  private useCaseSubscription: Subscription;
  /* tslint:enable*/
  @Input() usecases: UseCase[];

  graph: Graph;

  @Input() set components(components: GraphComponent[]) {
    this._components = components;
    if (this.graph && this._components.length > 0) {
      this.resetGraph();
    }
  }
  get components() {
    return this._components;
  }
  @Input() set relationships(relationships: Relationship[]) {
    this._relationships = relationships;
    this.setConnections();
    if (this.graph && this._relationships.length > 0) {
      this.resetGraph();
    }
  }
  @Input() set options(options: Options) {
    this._options = options;
    if (this.graph) {
      this.graph.initSimulation(this.options);
    }
  }
  get options() {
    return this._options;
  }
  getConnections() {
      return this.connections;
    }

  constructor(private d3Service: D3Service,
              private ref: ChangeDetectorRef,
              private useCaseDisplayService: UseCaseDisplayService) {}

  ngOnInit() {
    this.graph = this.d3Service.getForceDirectedGraph(this.components, this.getConnections(), this.usecases);

    this.graph.ticker.subscribe(() => {
      this.ref.markForCheck();
    });

    this.useCaseSubscription = this.useCaseDisplayService.getHighlightListener().subscribe( usecaseName => {
      this.toggleUseCase(usecaseName);
    });

  }

  ngAfterViewInit() {
    this.graph.initSimulation(this.options);
  }

  clearUseCase() {
    // reset the onPath flags for all components and relationships
    this.graph.components.forEach((component) => {
      component.onUseCasePath = false;
    });
    this.graph.connections.forEach((connection) => {
      connection.onUseCasePath = false;
      for (const relationship of connection.relationships) {
          relationship.onUseCasePath = false;
      }
    });
  }

  toggleUseCase(selectedUseCase: string) {
    if (selectedUseCase.match('none')) {

      // reset the onPath flags for all components and relationships
      this.clearUseCase();
    } else {
      let ucPath;

      // grab the path for the selected usecase
      this.graph.useCases.forEach((usecase) => {
        if (selectedUseCase.match(usecase.name)) {
          ucPath = usecase.path;
        }
      });

      if (ucPath === false) {
        // should never run into the case where the UC doesnt exist
        return;
      }

      this.clearUseCase();

      // update the onPath flag in the components/relationships that are along the path
      this.graph.components.forEach((component) => {
        if (ucPath.includes(component.name)) {
          component.onUseCasePath = true;
        }
      });
      this.graph.connections.forEach((connection) => {
        for (const relationship of connection.relationships) {
          if (ucPath.includes(relationship.name)) {
            connection.onUseCasePath = true;
            relationship.onUseCasePath = true;
          }
        }
      });
    }
  }

  private resetGraph() {
    this.graph = this.d3Service.getForceDirectedGraph(this.components, this.connections, this.usecases);

    this.graph.ticker.subscribe(() => {
      this.ref.markForCheck();
    });
    this.graph.initSimulation(this.options);
  }

  private setConnections() {
    const connectionpoints = [];
    const tempconnections = [];

    if (this._relationships.length > 0) {
      for (const outerrelationship of this._relationships) {
        const similarrelationships = [];
        const currsrc = outerrelationship.source;
        const currtarg = outerrelationship.target;
        const currpoint: ConnectionPoint = {source: currsrc, target: currtarg};

        if (connectionpoints.length === 0) {
          connectionpoints.push(currpoint);
        } else {
          let found = false;
          for (const point of connectionpoints) {
            if ((point.source === currsrc && point.target === currtarg)
              || (point.source === currtarg && point.target === currsrc)) {
              found = true;
              break;
            }
          }
          if (found === true) {
            continue;
          } else {
            connectionpoints.push(currpoint);
          }
        }

        let sourceToTarget = false;
        let targetToSource = false;
        for (const innerrelationship of this._relationships) {
          if (innerrelationship.source === currsrc && innerrelationship.target === currtarg) {
            similarrelationships.push(innerrelationship);
            sourceToTarget = true;
          } else if (innerrelationship.source === currtarg && innerrelationship.target === currsrc) {
            similarrelationships.push(innerrelationship);
            targetToSource = true;
          }
        }
        tempconnections.push(new Connection(similarrelationships, sourceToTarget && targetToSource));
      }
      this.connections = tempconnections;
    } else {
      this.connections = [];
    }
  }
}
