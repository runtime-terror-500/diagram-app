import {GraphComponent} from '../models/component.model';

export interface ConnectionPoint {
  source: number | GraphComponent;
  target: number | GraphComponent;
}
