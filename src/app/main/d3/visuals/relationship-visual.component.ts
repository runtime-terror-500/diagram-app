import {Component, Input, HostListener} from '@angular/core';
import {Relationship} from '../models/relationship.model';
import {Connection} from '../models/connection.model';
import {GraphComponent} from '../models/component.model';
import { MetadataDisplayService } from '../../metadata-display/metadata-display.service';

@Component({
  // tslint:disable-next-line
  selector: 'g[app-relationship-visual]',
  templateUrl: './relationship-visual.component.html',
  styleUrls: ['./shared-visual.component.scss']
})

export class RelationshipVisualComponent {
  @Input() connection: Connection;
  @Input() components: GraphComponent[];

  constructor(private metadataService: MetadataDisplayService) {  }

  @HostListener('click', ['$event'])
  onClick() {
    this.metadataService.displayConnection(this.connection, this.components);
  }
}
