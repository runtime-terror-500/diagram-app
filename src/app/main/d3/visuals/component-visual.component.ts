import {Component, Input, OnInit} from '@angular/core';
import { GraphComponent } from '../models/component.model';
import { MetadataDisplayService } from '../../metadata-display/metadata-display.service';
import { ParentService } from '../../parent/parent.service';

@Component({
  // tslint:disable-next-line
  selector: 'g[app-component-visual]',
  templateUrl: './component-visual.component.html',
  styleUrls: ['./shared-visual.component.scss']
})

export class ComponentVisualComponent implements OnInit {
  private MAX_WIDTH = 23;

  @Input() component: GraphComponent;
  displayName = '';

  constructor(private metadataService: MetadataDisplayService,
              private parentService: ParentService) { }

  ngOnInit() {
    if (this.component.name.length > this.MAX_WIDTH) {
      this.displayName = this.component.name.slice(0, this.MAX_WIDTH);
      this.displayName = this.displayName + '...';
    } else {
      this.displayName = this.component.name;
    }
  }

  onComponentClick() {
    this.metadataService.displayComponent(this.component);
  }

  expandChildren() {
    this.parentService.emitExpandButtonEvent(this.component.name);
  }
}
