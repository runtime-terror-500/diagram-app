import {Directive, ElementRef, Input, OnInit} from '@angular/core';
import {D3Service} from '../services/d3.service';

@Directive({
  selector: '[appZoomableSVG]'
})
export class ZoomableDirective implements OnInit {
  @Input() appZoomableSVG: ElementRef;

  constructor(private d3Service: D3Service, private elementRef: ElementRef) { }

  ngOnInit() {
    this.d3Service.applyZoomableBehaviour(this.appZoomableSVG, this.elementRef.nativeElement);
  }
}
