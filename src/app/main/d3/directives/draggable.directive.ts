import {Directive, ElementRef, Input, OnInit} from '@angular/core';
import {GraphComponent} from '../models/component.model';
import {Graph} from '../models/graph.model';
import {D3Service} from '../services/d3.service';

@Directive({
  selector: 'g[appDraggable]'
})
export class DraggableDirective implements OnInit {
  @Input() draggableComponent: GraphComponent;
  @Input() draggableGraph: Graph;

  constructor(private d3Service: D3Service, private elementRef: ElementRef) { }

  ngOnInit() {
    this.d3Service.applyDraggableBehaviour(this.elementRef.nativeElement, this.draggableComponent, this.draggableGraph);
  }
}
