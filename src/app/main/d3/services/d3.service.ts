import {Injectable} from '@angular/core';
import {Graph} from '../models/graph.model';
import {GraphComponent} from '../models/component.model';
import {Connection} from '../models/connection.model';
import * as d3 from 'd3';
import { UseCase } from '../models/usecase.model';

@Injectable({providedIn: 'root'})
export class D3Service {

  constructor() { }

  applyZoomableBehaviour(svgElement, containerElement) {
    const svg = d3.select(svgElement);
    const container = d3.select(containerElement);

    const zoomed = () => {
      const transform = d3.event.transform;
      container.attr('transform', 'translate(' + transform.x + ',' + transform.y + ') scale(' + transform.k + ')');
    };

    const zoom = d3.zoom().scaleExtent([0.25, 2]).on('zoom', zoomed);
    svg.call(zoom);
  }

  applyDraggableBehaviour(element, component: GraphComponent, graph: Graph) {
    const d3Element = d3.select(element);

    function started() {
      d3.event.sourceEvent.stopPropagation();

      if (!d3.event.active) {
        graph.simulation.alphaTarget(0.3).restart();
      }

      d3.event.on('drag', dragged).on('end', ended);

      function dragged() {
        component.fx = d3.event.x;
        component.fy = d3.event.y;
      }

      function ended() {
        if (!d3.event.active) {
          graph.simulation.alphaTarget(0);
        }
        component.fx = null;
        component.fy = null;
      }
    }
    d3Element.call(d3.drag().on('start', started));
  }

  getForceDirectedGraph(components: GraphComponent[], connections: Connection[], usecases: UseCase[]) {
    return new Graph(components, connections, usecases);
  }
}
