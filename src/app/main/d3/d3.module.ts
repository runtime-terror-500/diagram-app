import {NgModule} from '@angular/core';
import {ComponentVisualComponent} from './visuals/component-visual.component';
import {RelationshipVisualComponent} from './visuals/relationship-visual.component';
import {GraphVisualComponent} from './visuals/graph-visual.component';
import {D3Service} from './services/d3.service';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {DraggableDirective} from './directives/draggable.directive';
import {MaterialModule} from 'src/app/material.module';
import { ZoomableDirective } from './directives/zoomable.directive';

@NgModule({
  declarations: [
    ComponentVisualComponent,
    RelationshipVisualComponent,
    GraphVisualComponent,
    DraggableDirective,
    ZoomableDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule
  ],
  exports: [
    ComponentVisualComponent,
    RelationshipVisualComponent,
    GraphVisualComponent,
    DraggableDirective,
    ZoomableDirective
  ],
  providers: [
    D3Service
  ],
})
export class D3Module { }
