import {GraphComponent} from './component.model';
import {Connection} from './connection.model';
import {Relationship} from './relationship.model';
import {UseCase} from './usecase.model';
import {EventEmitter} from '@angular/core';
import {forceCenter, forceCollide, forceLink, forceManyBody, forceSimulation, Simulation} from 'd3-force';
import {Options} from '../../../shared/options.interface';

const FORCES = {
  LINKS: 250, // creating a fixed distance between connected nodes
  MIN_NEG_DISTANCE: 10, // TODO radius of comp to prevent overlap
  NEG_CHARGE: -1200, // rate components are repulsed
  POS_CHARGE: 500, // rate the components are attracted
  MAX_NEG_DISTANCE: 600,
  MIN_POS_DISTANCE: 1000
};

export class Graph {

  ticker: EventEmitter<Simulation<GraphComponent, Connection>> = new EventEmitter();
  simulation: Simulation<any, any>;

  components: GraphComponent[] = [];
  useCases: UseCase[] = [];
  connections: Connection[] = [];

  constructor(components: GraphComponent[], connections: Connection[], useCases: UseCase[]) {
    this.components = components;
    this.connections = connections;
    this.useCases = useCases;
  }

  initComponents() {
    if (!this.simulation) {
      // TODO throw error, sim not initialized
    }
    this.simulation.nodes(this.components);

  }

  initConnections() {
    if (!this.simulation) {
      // TODO throw error
    }
    // Must have 2 components to have relationships
    if (this.components && this.components.length >= 2) {
      this.simulation.force('link', forceLink().links(this.connections).distance(FORCES.LINKS));
    }
  }

  initSimulation(options: Options) {
    if (!this.simulation) {
      const ticker = this.ticker;

      const negCharge = forceManyBody().strength(FORCES.NEG_CHARGE)
                                       .distanceMin(FORCES.MIN_NEG_DISTANCE)
                                       .distanceMax(FORCES.MAX_NEG_DISTANCE);
      const posCharge = forceManyBody().strength(FORCES.POS_CHARGE)
                                       .distanceMin(FORCES.MIN_POS_DISTANCE);

      this.simulation = forceSimulation().alphaDecay(0.01).force('neg', negCharge).force('pos', posCharge);


      this.simulation.on('tick', function() {
        ticker.emit(this);
      });

      this.initComponents();
      this.initConnections();
    }
    this.simulation.force('center', forceCenter(options.width / 2, options.height / 2)); // center of graph
    this.simulation.restart(); // restart simulation internal timer
  }
}
