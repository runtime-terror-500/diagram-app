import {GraphComponent} from './component.model';

export class Relationship {
  name: string;
  description: string;
  source: number | GraphComponent;
  target: number | GraphComponent;
  metadata: object;

  onUseCasePath: boolean;

  constructor(name: string, description: string, source: number, target: number, metadata: object) {
    this.name = name;
    this.description = description;
    this.source = source;
    this.target = target;
    this.metadata = metadata;
    this.onUseCasePath = false;
  }
}
