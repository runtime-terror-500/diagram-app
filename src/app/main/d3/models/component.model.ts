import {SimulationNodeDatum} from 'd3-force';
import {UseCase} from './usecase.model';

export class GraphComponent implements SimulationNodeDatum {
  /* Required for D3 */
  index?: number;
  x = 1;
  y = 1;
  vx?: number;
  vy?: number;
  fx?: number | null;
  fy?: number | null;

  name: string;
  description: string;
  children: Array<string>;
  relationships: Array<string>;
  metadata: object;
  subGraph: SubGraph;

  onUseCasePath: boolean;
  mouseOver: boolean;
  hasChildren: boolean;

  constructor(name: string, description: string, children: Array<string>,
              relationships: Array<string>, metadata: object, subGraph?: SubGraph) {
    this.name = name;
    this.description = description;
    this.children = children;
    this.relationships = relationships;
    this.metadata = metadata;
    this.subGraph = subGraph;
    this.onUseCasePath = false;
    this.mouseOver = false;
    if (this.children.length > 0) {
      this.hasChildren = true;
    }
  }

  mouseOverEvent() {
    this.mouseOver = true;
  }

  mouseOutEvent() {
    this.mouseOver = false;
  }

  noChildren(): boolean {
    return !this.hasChildren;
  }
}

export interface SubGraph {
  components: Array<GraphComponent>;
  relationships: Array<string>;
  useCases: Array<UseCase>;
}
