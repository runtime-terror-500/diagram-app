import {GraphComponent} from './component.model';
import {SimulationLinkDatum} from 'd3-force';
import {Relationship} from './relationship.model';

export class Connection implements SimulationLinkDatum<GraphComponent> {
  index?: number;

  relationships: Relationship[];
  source: number | GraphComponent;
  target: number | GraphComponent;
  bidirectional: boolean;

  onUseCasePath: boolean;
  mouseOver: boolean;

  constructor(relationships: Relationship[], bidirectional: boolean) {
    this.relationships = relationships;
    this.source = this.relationships[0].source;
    this.target = this.relationships[0].target;
    this.onUseCasePath = false;
    this.mouseOver = false;
    this.bidirectional = bidirectional;
  }

  mouseOverEvent() {
    this.mouseOver = true;
  }

  mouseOutEvent() {
    this.mouseOver = false;
  }
}
