
export class UseCase {

  name: string;
  description: string;
  path: string[];
  metadata: object;

  onUseCasePath: boolean;

  constructor(name: string, description: string, path: string[], metadata: object) {
    this.name = name;
    this.description = description;
    this.path = path;
    this.metadata = metadata;
    this.onUseCasePath = true;
  }


}
