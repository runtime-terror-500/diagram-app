import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import {MonacoEditorModule} from 'ngx-monaco-editor';
import {FormsModule} from '@angular/forms';
import { UsecaseDisplayComponent } from './usecase-display.component';
import { MaterialModule } from 'src/app/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {UseCaseDisplayService} from './use-case-display.service';

describe('UseCaseDisplay', () => {
  let component: UsecaseDisplayComponent;
  let fixture: ComponentFixture<UsecaseDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsecaseDisplayComponent ],
      imports: [
        FormsModule,
        MaterialModule,
        BrowserAnimationsModule],
      providers: [
        UseCaseDisplayService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsecaseDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('subscribed component should receive emitted highlight event',
  inject([UseCaseDisplayService], (usecaseService) => {
    usecaseService.getHighlightListener().subscribe((message) => {
      expect(message).toBe('test');
    });
    usecaseService.emitHighlightEvent('test');

  }));
});
