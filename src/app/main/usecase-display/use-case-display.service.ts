import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({providedIn: 'root'})
export class UseCaseDisplayService {

  private updateUseCaseListener = new Subject<object[]>();
  private highlightUseCaseListener = new Subject<string>();

  constructor() { }

  getUseCaseListener() {
    return this.updateUseCaseListener.asObservable();
  }

  emitUpdateUseCaseEvent(useCases: object[]) {
    this.updateUseCaseListener.next(useCases);
  }

  getHighlightListener() {
    return this.highlightUseCaseListener.asObservable();
  }

  emitHighlightEvent(useCase: string) {
    this.highlightUseCaseListener.next(useCase);
  }
}
