import {Component, OnDestroy, OnInit} from '@angular/core';
import {ToolbarService} from '../../toolbar/toolbar.service';
import {Subject, Subscription} from 'rxjs';
import {ParentService} from '../parent/parent.service';
import {UseCaseDisplayService} from './use-case-display.service';
import {UseCase} from '../d3/models/usecase.model';

@Component({
  selector: 'app-usecase-display',
  templateUrl: './usecase-display.component.html',
  styleUrls: ['./usecase-display.component.scss']
})
export class UsecaseDisplayComponent implements OnInit, OnDestroy {

  private useCaseListener: Subscription;


  selection = new Subject();
  useCases: UseCase[];

  selectedUsecase = 'none';
  useCaseData: UseCase;
  objectKeys = Object.keys;


  constructor(private toolbarService: ToolbarService,
              private parentService: ParentService,
              private usecaseService: UseCaseDisplayService) {}


  displayUseCase() {
    this.usecaseService.emitHighlightEvent(this.selectedUsecase);
    this.useCases.forEach(uc => {
      if (uc.name === this.selectedUsecase) {
        this.useCaseData = uc;
      }
    });
  }
  ngOnInit() {
    this.parentService.getKeyUpListener().subscribe(yaml => {
      this.selectedUsecase = 'none';
    });
    this.useCaseListener = this.usecaseService.getUseCaseListener().subscribe(usecases => {
      this.useCases = usecases as unknown as UseCase[];
    });
  }

  ngOnDestroy() {
    if (this.useCaseListener) {
      this.useCaseListener.unsubscribe();
    }
  }
}
