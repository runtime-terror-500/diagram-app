import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentComponent } from './parent.component';
import {TextEditorComponent} from '../text-editor/text-editor.component';
import {DiagramComponent} from '../diagram/diagram.component';
import {MaterialModule} from '../../material.module';
import {MonacoEditorModule} from 'ngx-monaco-editor';
import {FormsModule} from '@angular/forms';
import {D3Module} from '../d3/d3.module';
import {MatTabsModule} from '@angular/material/tabs';

describe('ParentComponent', () => {
  let component: ParentComponent;
  let fixture: ComponentFixture<ParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DiagramComponent,
        ParentComponent,
        TextEditorComponent
      ],
      imports: [
        D3Module,
        FormsModule,
        MaterialModule,
        MatTabsModule,
        MonacoEditorModule.forRoot()
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
