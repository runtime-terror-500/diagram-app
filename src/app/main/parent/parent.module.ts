import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiagramComponent } from '../diagram/diagram.component';
import { ParentComponent } from './parent.component';
import { TextEditorComponent } from '../text-editor/text-editor.component';
import {MaterialModule} from '../../material.module';
import {FormsModule} from '@angular/forms';
import {MonacoEditorModule} from 'ngx-monaco-editor';
import {D3Module} from '../d3/d3.module';
import { MetadataDisplayComponent } from '../metadata-display/metadata-display.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {MatTabsModule} from '@angular/material/tabs';
import {UsecaseDisplayComponent} from '../usecase-display/usecase-display.component';

@NgModule({
    declarations: [
        DiagramComponent, // app component
        ParentComponent,
        TextEditorComponent,
        MetadataDisplayComponent,
        UsecaseDisplayComponent
    ],
    imports: [
        CommonModule,
        D3Module,
        FormsModule,
       FontAwesomeModule,
        MaterialModule,
        MonacoEditorModule.forRoot()
    ],
    exports: [
        ParentComponent
    ]
})
export class ParentModule { }
