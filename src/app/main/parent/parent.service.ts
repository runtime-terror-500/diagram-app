import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({providedIn: 'root'})
export class ParentService {

  private keyUpListener = new Subject<string>();
  private expandButtonListener = new Subject<string>();

  constructor() { }

  getKeyUpListener() {
    return this.keyUpListener.asObservable();
  }

  getExpandButtonListener() {
    return this.expandButtonListener.asObservable();
  }

  emitKeyUpEvent(yaml: string) {
    this.keyUpListener.next(yaml);
  }

  emitExpandButtonEvent(component: string) {
    this.expandButtonListener.next(component);
  }
}
