import {Component, ElementRef, HostListener, OnDestroy, OnInit} from '@angular/core';
import {GraphComponent, SubGraph} from '../d3/models/component.model';
import {Relationship} from '../d3/models/relationship.model';
import {UseCase} from '../d3/models/usecase.model';
import {Options} from '../../shared/options.interface';
import {ParentService} from '../parent/parent.service';
import {Subscription} from 'rxjs';
import {faArrowLeft, faExclamationTriangle} from '@fortawesome/free-solid-svg-icons';
import Graph from '@runtimeterror/rt-lib/dist/lib/graph';
import GraphError from '@runtimeterror/rt-lib/dist/lib/graph-error';
import {UseCaseDisplayService} from '../usecase-display/use-case-display.service';

@Component({
  selector: 'app-diagram',
  templateUrl: './diagram.component.html',
  styleUrls: ['./diagram.component.scss']
})
export class DiagramComponent implements OnInit, OnDestroy {

  private parentServiceSubscription: Subscription;
  private expandButtonSubscription: Subscription;

  private previousYaml: string;

  components: GraphComponent[] = [];
  relationships: Relationship[] = [];
  useCases: UseCase[] = [];

  topLevel: TopLevelGraph;
  showCollapseButton = false;
  collapseButtonName: string;

  showError = false;
  graphErrors: GraphError[] = [];

  // font awesome icons
  warning = faExclamationTriangle;
  arrow = faArrowLeft;

  options: Options = {
    width: null,
    height: null
  };

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.options.width = this.element.nativeElement.offsetParent.offsetWidth;
    this.options.height = this.element.nativeElement.offsetParent.offsetHeight;
  }

  constructor(private element: ElementRef,
              private parentService: ParentService,
              private useCaseDisplayService: UseCaseDisplayService) {}

  ngOnInit() {
    this.parentServiceSubscription = this.parentService.getKeyUpListener().subscribe(yaml => {
      yaml = yaml.trim(); // remove whitespace from both sides
      if (typeof yaml !== 'undefined' && yaml && this.previousYaml !== yaml) { // if yaml is same, don't call lib
        this.previousYaml = yaml;

        Graph.build(yaml).then((result: Graph) => {
          this.showError = false;
          this.showCollapseButton = false;
          this.addComponents(result.components);
          this.addRelationships(result.relationships);
          this.addUseCases(result.useCases);
        }, (error: GraphError[]) => {
          this.showError = true;
          this.components = [];
          this.relationships = [];
          this.graphErrors = error;
        });
      }
    });

    this.expandButtonSubscription = this.parentService.getExpandButtonListener().subscribe(name => {
      const subGraph = this.getSubGraph(name);
      this.collapseButtonName = name;
      if (subGraph) {
        this.expandSubGraph(subGraph);
      }
    });

    this.options.width = this.element.nativeElement.offsetParent.offsetWidth;
    this.options.height = this.element.nativeElement.offsetParent.offsetHeight;
  }

  collapseButtonClick() {
    this.showCollapseButton = false;
    this.displayTopLevel();
  }

  private expandSubGraph(subGraph) {
    this.topLevel = {
      components: Array.from(this.components),
      relationships: Array.from(this.relationships),
      useCases: Array.from(this.useCases)
    };
    this.addComponents(Array.from(subGraph.components));
    this.addRelationships(Array.from(subGraph.relationships));
    this.addUseCases(Array.from(subGraph.useCases));
    this.showCollapseButton = true;
  }

  private displayTopLevel() {
    this.addComponents(this.topLevel.components);
    this.addDefinedRelationships(this.topLevel.relationships);
    this.addUseCases(this.topLevel.useCases);
    this.topLevel = null;
  }

  private addComponents(components) {
    this.components = [];
    for (const component of components) {
      this.components.push(new GraphComponent(
        component.name,
        component.description,
        component.children,
        component.relationships,
        component.metadata,
        component.subGraph
      ));
    }
  }

  /**
   * Use this method when relationships have already been defined by d3
   * (ie source and target are GraphComponents not strings)
   */
  private addDefinedRelationships(relationships) {
    this.relationships = [];
    for (const relationship of relationships) {
      this.relationships.push(new Relationship(relationship.name, relationship.description,
        relationship.source, relationship.target, relationship.metadata));
    }
  }

  private addRelationships(relationships) {
    this.relationships = [];
    for (const relationship of relationships) {
      const from = this.components.findIndex(i => i.name === relationship.from);
      const to = this.components.findIndex(i => i.name === relationship.to);
      this.relationships.push(new Relationship(relationship.name, relationship.description, from, to, relationship.metadata));
    }
  }

  private addUseCases(useCases) {
    this.useCases = [];
    for (const useCase of useCases) {
      this.useCases.push(new UseCase(useCase.name, useCase.description, useCase.path, useCase.metadata));
    }
    this.useCaseDisplayService.emitUpdateUseCaseEvent(this.useCases);
  }

  private getSubGraph(parent: string): SubGraph {
    const parentComponent = this.components.find(component => component.name === parent);
    return parentComponent.subGraph;
  }

  ngOnDestroy() {
    this.parentServiceSubscription.unsubscribe();
    this.expandButtonSubscription.unsubscribe();
  }
}

interface TopLevelGraph {
  components: GraphComponent[];
  relationships: Relationship[];
  useCases: UseCase[];
}
