import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ParentModule } from './main/parent/parent.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToolbarComponent } from './toolbar/toolbar.component';
import {MaterialModule} from './material.module';
import {FormsModule} from '@angular/forms';
import {D3Module} from './main/d3/d3.module';
import { SplashComponent } from './splash/splash.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    SplashComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    D3Module,
    ParentModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
