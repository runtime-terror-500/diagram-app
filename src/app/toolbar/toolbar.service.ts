import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import * as FileSaver from 'file-saver';


@Injectable({providedIn: 'root'})
export class ToolbarService {

  private fileUploadListener = new Subject<string>();
  private fileDownloadListener = new Subject<boolean>();

  constructor() { }

  downloadFile(file: string) {
    const blob = new Blob([file]);
    FileSaver.saveAs(blob, 'diagram-app.yaml');
  }

  getFileUploadListener() {
    return this.fileUploadListener.asObservable();
  }

  getFileDownloadListener() {
    return this.fileDownloadListener.asObservable();
  }

  downloadFileAction() {
    this.fileDownloadListener.next(true);
  }

  uploadedFile(file: string) {
    this.fileUploadListener.next(file);
  }
}
