import { Component, OnInit } from '@angular/core';
import {ToolbarService} from './toolbar.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  constructor(private toolbarService: ToolbarService) { }

  ngOnInit() {
  }

  filePicked(event: Event) {
    const fileName = (event.target as HTMLInputElement).files[0];

    const reader = new FileReader();
    reader.onload = () => {
      // console.log(reader.result as string);
      this.toolbarService.uploadedFile(reader.result as string);
    };
    reader.readAsText(fileName);

  }

  downloadYAML() {
    this.toolbarService.downloadFileAction();
  }

}
